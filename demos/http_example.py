#!/usr/bin/env python3
from http.server import *
import http.client

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)

    for _ in range(10):
        conn = http.client.HTTPConnection("localhost", 8000)
        conn.request("GET", "/")
        httpd.handle_request()

if __name__ == "__main__":
    run()
