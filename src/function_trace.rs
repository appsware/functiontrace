use serde::Deserialize;
use std::time::Duration;

/// A representation of the various trace messages a client thread can send us during normal
/// tracing operation.
#[derive(Deserialize, Debug)]
#[serde(tag="type")]
pub enum FunctionTrace {
    /// A request to register this as a new thread.
    ///
    /// **NOTE**: This must be sent as the first message from a new thread, and may not be sent
    /// twice by any thread.
    RegisterThread(ThreadRegistration),
    Call {
        time: Duration,
        func_name: String,
        filename: String,
        linenumber: u32,
    },
    Return {
        time: Duration,
        func_name: String
    },
    NativeCall {
        time: Duration,
        func_name: String,
        module_name: String,
    },
    NativeReturn {
        time: Duration,
        func_name: String,
    },
    Exception {
        time: Duration,
        exception_type: String,
        exception_value: String,
        filename: String,
        linenumber: u32,
    },
    Log {
        time: Duration,
        log_type: String,
        log_value: String,
    },
    Import {
        time: Duration,
        module_name: String,
    },
    Allocation {
        time: Duration,
        details: AllocationDetails,
    }
}

/// Information about allocations.
#[derive(Deserialize, Debug)]
#[serde(tag="type")]
pub enum AllocationDetails {
    /// The amount and location of a new allocation
    Alloc {
        bytes: usize,
        addr: usize,
    },
    /// The new size of a reallocation from `old_addr` to `new_addr`.
    Realloc {
        bytes: usize,
        old_addr: usize,
        new_addr: usize,
    },
    /// The address that was `free()`ed.
    Free {
        old_addr: usize
    }
}

/// Information relevant for initializing a trace.
#[derive(Deserialize, Debug)]
pub struct TraceInitialization {
    /// The name (typically based off of argv) of the program initializing the trace (ex:
    /// `hello.py world`).
    pub program_name: String,
    /// The version information of the `functiontrace` client talking to this server (ex:
    /// `py-functiontrace 0.3.0`).
    pub program_version: String,
    /// The version for the underlying language the program is running on (ex: `Python 3.7.1`).
    pub lang_version: String,
    /// The general operating system platform the program is running on (ex: `darwin`).
    pub platform: String,
    /// An opaque system time that all other client-sent times will be relative to.
    pub time: Duration,
}

/// This message contains information for registering threads (including a program's main thread).
#[derive(Deserialize, Debug)]
pub struct ThreadRegistration {
    /// A system time for when this thread registered itself as being traced with FunctionTrace.
    pub time: Duration,
    /// The program name this thread corresponds to.
    pub program_name: String,
    /// The process this thread belongs to.
    pub pid: usize,
}
